package com.example.assignment13.model

import com.google.gson.annotations.SerializedName
import com.squareup.moshi.Json

data class News(
    @field:Json(name = "created_at")
    val createdAt: Any,
    val date: String,
    val deleted_at: Any,
    val description: String,
    val id: Int,
    val images: String,
    @field:Json(name = "img-url")
    val tmgUrl : String,
    val title: String,
    @field:Json(name = "updated_at")
    val updatedAt: Any
) {}