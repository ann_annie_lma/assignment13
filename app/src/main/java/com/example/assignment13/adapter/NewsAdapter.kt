package com.example.assignment13.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.assignment13.R
import com.example.assignment13.databinding.NewsItemBinding
import com.example.assignment13.model.News

class NewsAdapter : RecyclerView.Adapter<NewsAdapter.PropertyViewHolder>() {



    inner class PropertyViewHolder(val binding: NewsItemBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun bind()
        {
            binding.apply {
                val news = resultNews[adapterPosition]
                tvDate.text = news.date
                tvTitle.text = news.title
                tvDescription.text = news.description

                Glide.with(ivCover.getContext())  // glide with this view
                    .load(news.tmgUrl)
                    .override(250, 250) // set this size
                    .placeholder(R.mipmap.ic_launcher) // default placeHolder image
                    .error(R.mipmap.ic_launcher)
                    .centerCrop()// use this image if faile
                    .into(ivCover)

            }
        }
    }


    private val diffCallBack = object: DiffUtil.ItemCallback<News>()
    {
        override fun areItemsTheSame(oldItem: News, newItem: News): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: News, newItem: News): Boolean {
            return oldItem == newItem
        }

    }

    private val differ = AsyncListDiffer(this,diffCallBack)
    var resultNews : List<News>
        get() = differ.currentList
        set(value) {differ.submitList(value)}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PropertyViewHolder {
        return PropertyViewHolder(
            NewsItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ))
    }

    override fun onBindViewHolder(holder: PropertyViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = resultNews.size
}