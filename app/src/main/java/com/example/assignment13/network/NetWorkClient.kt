package com.example.assignment13.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

object NetWorkClient {

    val api:ApiService by lazy {
        Retrofit.Builder().
        baseUrl(NetworkConstants.BASE_URL).
        addConverterFactory(MoshiConverterFactory.create()).
        build().create(ApiService::class.java)

    }
}