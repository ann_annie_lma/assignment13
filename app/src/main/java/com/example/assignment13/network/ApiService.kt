package com.example.assignment13.network

import com.example.assignment13.model.News
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET(NetworkConstants.REPROSITORY_URL)
    suspend fun getData(): Response<List<News>>
}