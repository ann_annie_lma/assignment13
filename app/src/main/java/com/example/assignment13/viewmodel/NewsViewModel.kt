package com.example.assignment13.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.assignment13.model.News
import com.example.assignment13.network.ApiService
import com.example.assignment13.network.NetWorkClient
import com.example.assignment13.network.NetworkConstants
import com.example.assignment13.network.Resource
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class NewsViewModel : ViewModel() {

    private var _response:MutableLiveData<Resource<List<News>>> = MutableLiveData()
    val response : LiveData<Resource<List<News>>> get() = _response

    fun load()
    {
        viewModelScope.launch {
            withContext(IO)
            {
                getNews()
            }
        }
    }


    private suspend fun getNews()
    {

       try {
           val result = NetWorkClient.api.getData()
           val resultBody = result.body()
           if(result.code() == 200)
           {
               _response.postValue(Resource.Success(resultBody))
           }
           else
           {
               _response.postValue(Resource.Failure(result.message()))
           }

       } catch (e:Exception)
       {
           _response.postValue(Resource.Failure(e.message!!))
       }
    }

}