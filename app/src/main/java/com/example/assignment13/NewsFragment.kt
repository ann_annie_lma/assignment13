package com.example.assignment13

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assignment13.adapter.NewsAdapter
import com.example.assignment13.databinding.FragmentNewsBinding
import com.example.assignment13.network.Resource
import com.example.assignment13.viewmodel.NewsViewModel

class NewsFragment : BaseFragment<FragmentNewsBinding>(FragmentNewsBinding::inflate){

    private val newsViewModel : NewsViewModel by viewModels()
    lateinit var newsAdapter: NewsAdapter

    override fun start() {
        setUpRecycler()
        observers()
        newsViewModel.load()
    }


    private fun setUpRecycler()
    {
        binding.rvNews.apply {
            newsAdapter = NewsAdapter()
            adapter = newsAdapter
            layoutManager = LinearLayoutManager(context)

        }
    }

    private fun observers()
    {
        newsViewModel.response.observe(viewLifecycleOwner, {response ->
            binding.pbNews.isVisible = true
            when(response)
            {
                is Resource.Success ->
                {
                    newsAdapter.resultNews = response.data!!
                    binding.pbNews.isVisible = false
                }
                is Resource.Failure ->
                {
                    Toast.makeText(context,"Error",Toast.LENGTH_SHORT).show()
                    binding.pbNews.isVisible = false
                }
                is Resource.Loading ->
                {

                }
            }
        })
    }

}